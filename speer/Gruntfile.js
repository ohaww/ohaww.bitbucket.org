var path = require('path');

module.exports = function(grunt) {
   'use strict';

   grunt.initConfig({
      webfont: {
         icons: {
            src: 'icons/*.svg',
            dest: 'icons/fonts',
            options: {
               font: "ww-hc-icons",
               syntax: "bootstrap",
               templateOptions: {
                  baseClass: 'ww-hc-icon',
                  classPrefix: 'ww-hc_',
                  mixinPrefix: 'ww-hc-'
               }
            }
         }
      }
   });

   grunt.loadNpmTasks('grunt-webfont');

};
