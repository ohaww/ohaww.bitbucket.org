/*
 * jQuery v1.9.1 included
 */
$(document).ready(function () {

   //add id attribute to body
   $("body").attr({id: "body"});

   //add current category to body tag as class
   $("body").addClass($("nav.sub-nav>ol.breadcrumbs>li:nth(1)").text().trim().replace(/ /g, "-").toLowerCase());

   // clear search button text, gets replaced with icon
   $(".search>input[type=submit]").attr({value: ""});

   // if a mad article is present
   if ($("article").length && $(".mad-article").length) {
      // (reset) hide all article dropdown body elements
      $(".MCDropDownBody").attr({style: "display:none"});

      // build sidebar article navigation html
      $("article>h1, .mad-article>h2, .mad-article>h3")
         .attr("id", function (index) {
            var a, id, h;
            a = $(this).text().trim();
            id = a.toLowerCase().replace(/ /g, "-");
            h = $(this).prop('tagName').toLowerCase();

            // add header navigation link to menu
            $(".mad-menu").append("<li><a href=#mad-" + id + " class=" + h + ">" + a + "</a></li>");

            // add id link to article headers
            return "mad-" + id;
         });
      // assign affix-top class
      $('.spy').affix({
         offset: {
            top: $('.spy').offset().top - 30,
            bottom: $('.article-column').offset().bottom
         }
      });
   }
});

$(document).ready(function () {

   // toggle categories and sections on the home page

//FDE  $(".category-tree").on("click", "h2 a", function() {
//FDE    $(this).parent().nextAll().toggle();
//FDE    return false;
//FDE  });

   // social share popups
   $(".share a").click(function (e) {
      e.preventDefault();
      window.open(this.href, "", "height = 500, width = 500");
   });

   // toggle the share dropdown in communities
   $(".share-label").on("click", function (e) {
      e.stopPropagation();
      var isSelected = this.getAttribute("aria-selected") == "true";
      this.setAttribute("aria-selected", !isSelected);
      $(".share-label").not(this).attr("aria-selected", "false");
   });

   $(document).on("click", function () {
      $(".share-label").attr("aria-selected", "false");
   });

   // show form controls when the textarea receives focus
   $(".answer-body textarea").one("focus", function () {
      $(".answer-form-controls").show();
   });

   $(".comment-container textarea").one("focus", function () {
      $(".comment-form-controls").show();
   });

});

// Add images to the Section tree
$(document).ready(function () {

   var $section = $('.section-tree .section h3 a').each(function () {

      url = $(this).attr('href');

      val = url.substring(url.lastIndexOf('/') + 1).toLowerCase().replace(/\d+/g, '').substring(1);

      sectionImage = '<img src="//p4.zdassets.com/hc/theme_assets/299558/200014303/' + val + '.png" style="width: 40px;padding-right:5px" onError="this.src =\'//p4.zdassets.com/hc/theme_assets/299558/200014303/empty.png\'">';

      $(this).prepend(sectionImage);

   });

});

// Add images to the Categories tree
$(document).ready(function () {

   var $section = $('.category-tree .category h2 a').each(function () {

      url = $(this).attr('href');

      val = url.substring(url.lastIndexOf('/') + 1).toLowerCase().replace(/\d+/g, '').substring(1);

      sectionImage = '<img src="//p4.zdassets.com/hc/theme_assets/299558/200014303/' + val + '.png" style="width: 60px;padding-right:5px" onError="this.src =\'//p4.zdassets.com/hc/theme_assets/299558/200014303/empty.png\'">';

      $(this).prepend(sectionImage);

   });

});

// Rename Topics to Forum List
$(document).ready(function () {

   var elm = $('.community-nav').children('ul').children('li')[0];
   var text = 'Forum list';

   if ($(elm).has('a').length >= 1) {
      $($(elm).children('a')).text(text);
   }
   else {
      $(elm).text(text);
   }
});

$(document).ready(function() {
   //$('.request_ticket_form_id').hide();
});
