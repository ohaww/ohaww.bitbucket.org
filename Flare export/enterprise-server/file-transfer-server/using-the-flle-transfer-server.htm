<!DOCTYPE html>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" lang="en-us" xml:lang="en-us" class="no-feedback" data-mc-search-type="Stem" data-mc-help-system-file-name="Default.xml" data-mc-path-to-help-system="../../../../" data-mc-target-type="WebHelp2" data-mc-runtime-file-type="Topic" data-mc-preload-images="false" data-mc-in-preview-mode="false" data-mc-toc-path="Enterprise Server 9 documentation|File Transfer Server" data-mc-conditions="global.Enterprise Server 9">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../../../../Skins/Default/Stylesheets/TextEffects.css" rel="stylesheet" type="text/css" />
        <link href="../../../../Skins/Default/Stylesheets/Topic.css" rel="stylesheet" type="text/css" /><title>Using the File Transfer Server in Enterprise Server 9</title>
        <link href="../../../resources/stylesheets/modern.css" rel="stylesheet" type="text/css" />
        <script src="../../../../Resources/Scripts/jquery.min.js" type="text/javascript">
        </script>
        <script src="../../../../Resources/Scripts/plugins.min.js" type="text/javascript">
        </script>
        <script src="../../../../Resources/Scripts/MadCapAll.js" type="text/javascript">
        </script>
    </head>
    <body>
        <h1>Using the File Transfer Server in Enterprise Server 9</h1>
        <p class="NoSpaceAfter"><a href="#The_File_Transfer_Server" class="MCXref xref">The File Transfer Server</a>
        </p>
        <p class="NoSpaceBeforeOrAfter"><a href="#The_File_Transfer_folder" class="MCXref xref">The File Transfer folder</a>
        </p>
        <p class="NoSpaceBefore"><a href="#Deployment" class="MCXref xref">Deployment</a>
        </p>
        <p class="note"><b>Note:</b> The File Transfer Server is supported by Content Station 8 or higher; it is not yet supported by Smart Connection.</p>
        <p>The File Transfer Server  separates files from operational data during an upload or download process. This results in a higher performance and lower memory footprint compared to the combined method used in Enterprise Server versions 7 and lower.</p>
        <p>Client applications talk to the Server through Web services. During this process, workflow operation data is sent back and forth, but files are also uploaded and downloaded. The size of the files can be very big and can often contain binary data—unlike the operational data which is mostly very small in size and contains structured data.</p>
        <h2>How it used to be</h2>
        <p>Before Enterprise Server 8.0,  only one method of transferring files to client applications existed, namely through a protocol named DIME. Files and workflow operation data traveled together in one large (DIME) package over the same HTTP connection. This method had a significant performance hit and left a memory footprint on both the client and the server. This caused significant delays in processing time, directly resulting in an increased waiting time for end users.</p>
        <h2>How it is now</h2>
        <p>The additional File Transfer Server method which was introduced by Enterprise Server 8 separates the files from the workflow operational data and each flow has its own HTTP connection. Files are uploaded to (or downloaded from) the File Transfer Server and the workflow operational data is communicated directly to the Enterprise Server. When client and Server are talking Web services, there is no longer a need to work through a large DIME package to understand what the other side is talking about. This makes the File Transfer Server method much faster than the DIME method, together with a much lower memory footprint which avoids memory swapping.</p>
        <p>
            <img src="../../../resources/images/server/enterprise-server-file-transfer-workflow.png" alt="Enterprise Server file transfer workflow overview" />
        </p>
        <p class="ImageCaption">Figure:&#160;Workflow of uploading a file using a version 7 client application (file travels together with request) and a version 9 client application (file and request travel separately)</p>
        <p>Wether the MIME method or the File Transfer Server method is used is determined by the client application. </p>
        <p class="example"><b>Example:</b> Smart Connection 8 is still using DIME while Content Station 9 is already using the File Transfer Server. Because of the performance reasons mentioned, all client applications are encouraged to use the File Transfer Server during the Enterprise 9 time line.</p>
        <h2>How it will be</h2>
        <p>In the near future, the File Transfer Server enables client applications to minimize the user waiting times by:</p>
        <ul>
            <li class="BulletLevel1" value="1">Uploading/downloading files asynchronously and/or in parallel.</li>
            <li class="BulletLevel1" value="2">Skipping downloading files that are cached locally.</li>
            <li class="BulletLevel1" value="3">Reporting errors from the service before uploading or downloading (large) files.</li>
            <li class="BulletLevel1" value="4">Uploading a large file while the users is filling in the workflow dialog.</li>
        </ul>
        <h3>Schematic overview</h3>
        <p>The following figure shows a simplified overview of a client application uploading a file to Enterprise Server using the File Transfer Server:</p>
        <p>
            <img src="../../../resources/images/server/enterprise-server-transfer-process-simplified.png" alt="Enteprise Server transfer process simplified" style="margin-right: 20px;" />
        </p>
        <p class="ImageCaption">Figure: A simplified overview of the transfer process when a file is uploaded through the File Transfer Server.</p>
        <p><b>1.</b> The client application uploads the file to the File Transfer Server. <b>2.</b> The File Transfer Server puts the file in the File Transfer Folder. <b>3. </b>Enterprise Server retrieves the file. <b>4.</b> Enterprise Server puts the file in the File Store.</p>
        <h2><a name="The_File_Transfer_Server"></a>The File Transfer Server</h2>
        <p>Enterprise Server can play the role of an Application Server but also the role of a File Transfer Server. Physically they are the same: they originate from the same package and they are installed in the same way. Logically, once installed, they can change role any time just by configuration. Enterprise Server can even play both roles at the same time (this is the way in which Enterprise Server is configured by default).</p>
        <p>Even when the File Transfer Server runs within Enterprise Server on the same machine and Web location, its performance is much higher compared to the DIME file transfer method used in previous versions.</p>
        <p>Should there be a need to further increase performance (for instance for large systems with many Server machines where load balancing is important) the File Transfer Server can be moved to its own machine.</p>
        <p>It is possible to run multiple Enterprise Servers and multiple File Transfer Servers, all working together serving a group of client machines.</p>
        <p class="note"><b>Note:</b>&#160;The File Transfer Server must be seen as a communication line; features like image processing are therefore not handled by the Transfer Server.</p>
        <h3>Defining the File Transfer Server location</h3>
        <p>Defining the location of the File Transfer Server is done through the following options:</p>
        <ul>
            <li value="1"><b>File:</b> configserver.php file </li>
            <li value="2"><b>Name of option:</b> HTTP_FILE_TRANSFER_REMOTE_URL</li>
            <li value="3"><b>Possible values:</b> URL to the File Transfer Server, as seen from the point of view of the client machine.</li>
            <li value="4"><b>Default setting:</b> SERVERURL_ROOT.INETROOT.’/transferindex.php’</li>
            <li value="5"><b>Example:</b> <![CDATA[ ]]></li>
        </ul>
        <p class="exampleLevel1"><code>define( 'HTTP_FILE_TRANSFER_REMOTE_URL', SERVERURL_ROOT.INETROOT.'/transferindex.php' );</code><code>&#160;</code>
        </p>
        <ul>
            <li value="1"><b>File:</b> configserver.php file </li>
            <li value="2"><b>Name of option:</b> HTTP_FILE_TRANSFER_LOCAL_URL</li>
            <li value="3"><b>Possible values:</b>  URL to the File Transfer Server, as seen from the point of view  of the server machine.</li>
            <li value="4"><b>Default setting:</b> LOCALURL_ROOT.INETROOT.’/transferindex.php’</li>
            <li value="5"><b>Example:</b> <![CDATA[ ]]></li>
        </ul>
        <p class="exampleLevel1"><code>define( 'HTTP_FILE_TRANSFER_LOCAL_URL', LOCALURL_ROOT.INETROOT.'/transferindex.php' );</code><code>&#160;</code>
        </p>
        <h2><a name="The_File_Transfer_folder"></a>The File Transfer folder</h2>
        <p>Regardless of the deployment you find most suitable, make sure that the File Transfer Folder is located on the same disk as the File Store folder. This significantly improves performance because files are then moved between both folders instead of being copied (and deleted) which is much slower.</p>
        <p class="note"><b>Note:&#160;</b>For each File Transfer Server, only one File Transfer Folder can be set up. It is therefore not possible to work with dedicated File Transfer Folders for specific File Transfer Servers.</p>
        <h3>Defining the File Transfer Server location</h3>
        <p>Defining the location of the File Transfer Folder is done through the following options:</p>
        <ul>
            <li value="1"><b>File:</b> configserver.php file </li>
            <li value="2"><b>Name of option:</b> FILE_TRANSFER_REMOTE_PATH</li>
            <li value="3"><b>Possible values:</b> Path to the transfer folder, accessible from all  Enterprise Servers.</li>
            <li value="4"><b>Default setting:</b> WOODWINGSYSTEMDIRECTORY.’/TransferServerCache’</li>
            <li value="5"><b>Example:</b> <![CDATA[ ]]></li>
        </ul>
        <p class="exampleLevel1"><code>define( 'FILE_TRANSFER_REMOTE_PATH', WOODWINGSYSTEMDIRECTORY.'/TransferServerCache' );</code><code>&#160;</code>
        </p>
        <ul>
            <li value="1"><b>File:</b> configserver.php file </li>
            <li value="2"><b>Name of option:</b> FILE_TRANSFER_LOCAL_PATH</li>
            <li value="3"><b>Possible values:</b> Path to the transfer folder accessible from the Transfer Server.</li>
            <li value="4"><b>Default setting:</b> WOODWINGSYSTEMDIRECTORY.’/TransferServerCache’</li>
            <li value="5"><b>Example:</b> <![CDATA[ ]]></li>
        </ul>
        <p class="exampleLevel1"><code>define( 'FILE_TRANSFER_LOCAL_PATH', WOODWINGSYSTEMDIRECTORY.'/TransferServerCache' );</code><code>&#160;</code>
        </p>
        <h2><a name="Deployment"></a>Deployment</h2>
        <p>For a full description of the various ways of deploying the File Transfer Server, see <a href="../deployment/enterprise-server-deployment.htm" class="MCXref xref">Enterprise Server deployment</a>.</p>
        <p class="note"><b>Note:</b>&#160;It is recommended to choose a setup whereby the File Transfer folder is located on the same (logical) volume as where the File Store is located (as defined by the ATTACHMENTDIRECTORY setting in the config.php file). This is the case for a default installation where the FILE_TRANSFER_LOCAL_PATH setting points to a subdirectory of the  ATTACHMENTDIRECTORY.</p>
        <div class="MCRelationshipsProxy_0">
            <p class="taskHeading_task_0">Related Tasks</p>
            <p class="taskItem_task_0"><a href="setting-up-a-file-transfer-server.htm">Setting up a File Transfer Server</a>
            </p>
            <p class="taskItem_task_0"><a href="securing-file-transfer-server-through-https.htm">Securing the File Transfer Server through HTTPS</a>
            </p>
            <p class="taskItem_task_0"><a href="file-transfer-server-installation-on-another-system.htm">Installing the File Transfer Server on another system</a>
            </p>
            <p class="taskItem_task_0"><a href="transfer-server-folder-installation-on-another-system.htm">Installing the Transfer Server Folder on another system</a>
            </p>
            <p class="referenceHeading_reference_0">Reference Materials</p>
            <p class="referenceItem_reference_0"><a href="../configuration/enterprise-server-configuration-file-locations.htm">Location of the Enterprise Server 9 configuration files</a>
            </p>
            <p class="referenceItem_reference_0"><a href="../contents.htm">Enterprise Server 9 documentation</a>
            </p>
            <p class="referenceItem_reference_0"><a href="../concept/enterprise-concept.htm">The Enterprise concept</a>
            </p>
        </div>
    </body>
</html>