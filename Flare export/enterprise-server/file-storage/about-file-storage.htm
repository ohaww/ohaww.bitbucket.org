<!DOCTYPE html>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" lang="en-us" xml:lang="en-us" class="no-feedback" data-mc-search-type="Stem" data-mc-help-system-file-name="Default.xml" data-mc-path-to-help-system="../../../../" data-mc-target-type="WebHelp2" data-mc-runtime-file-type="Topic" data-mc-preload-images="false" data-mc-in-preview-mode="false" data-mc-toc-path="Enterprise Server 9 documentation|File Storage" data-mc-conditions="global.Enterprise Server 9">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../../../../Skins/Default/Stylesheets/TextEffects.css" rel="stylesheet" type="text/css" />
        <link href="../../../../Skins/Default/Stylesheets/Topic.css" rel="stylesheet" type="text/css" /><title>About file storage in Enterprise Server 9</title>
        <link href="../../../resources/tablestyles/example.css" rel="stylesheet" data-mc-stylesheet-type="table" />
        <link href="../../../resources/stylesheets/modern.css" rel="stylesheet" type="text/css" />
        <link href="../../../resources/tablestyles/example.css" rel="stylesheet" data-mc-stylesheet-type="table" />
        <script src="../../../../Resources/Scripts/jquery.min.js" type="text/javascript">
        </script>
        <script src="../../../../Resources/Scripts/plugins.min.js" type="text/javascript">
        </script>
        <script src="../../../../Resources/Scripts/MadCapAll.js" type="text/javascript">
        </script>
    </head>
    <body>
        <h1>About file storage in Enterprise Server 9</h1>
        <p class="NoSpaceAfter"><a href="#File_name_conventions" class="MCXref xref">File name conventions</a>
        </p>
        <p class="NoSpaceBeforeOrAfter"><a href="#Version_numbering" class="MCXref xref">Version numbering</a>
        </p>
        <p class="NoSpaceBeforeOrAfter"><a href="#Object_types" class="MCXref xref">Object types</a>
        </p>
        <p class="NoSpaceBeforeOrAfter"><a href="#Performance" class="MCXref xref">Performance</a>
        </p>
        <p class="NoSpaceBeforeOrAfter"><a href="#External_storage" class="MCXref xref">External storage</a>
        </p>
        <p class="BulletLevel1ContinuationNoSpaceBeforeAndAfter"><a href="#External_File_Store_configuration" class="MCXref xref">External File Store configuration</a>
        </p>
        <p class="BulletLevel1ContinuationNoSpaceBeforeAndAfter"><a href="#High-resolution_adverts_storage" class="MCXref xref">High-resolution adverts storage</a>
        </p>
        <p class="BulletLevel1ContinuationNoSpaceBeforeAndAfter"><a href="#3rd-Party_content_sources" class="MCXref xref">3rd-Party content sources</a>
        </p>
        <p class="NoSpaceBefore"><a href="#Storing_layouts_and_articles_locally" class="MCXref xref">Storing layouts and articles locally or always on the Server</a>
        </p>
        <p>In Enterprise Server, files are stored in a file system, including on an external drive in situations where disk space is at a premium.</p>
        <p>This article describes the way in which files are stored, how to set up external storage and how to manage the process of locally storing files by client applications.</p>
        <h2><a name="File_name_conventions"></a>File name conventions</h2>
        <p>When a file is stored in the File Store, the file name that is used is taken from one of two methods, each using its own convention:</p>
        <ol>
            <li value="1">For files directly belonging to an object:</li>
        </ol>
        <p class="BulletLevel2Continuation">&lt;object ID&gt;-[native|preview|output|thumb].v&lt;major&gt;.&lt;minor&gt;</p>
        <p class="exampleLevel1">File name example: 329-native.v0.1</p>
        <ol start="2">
            <li value="2">For files belonging to pages of a layout object:</li>
        </ol>
        <p class="BulletLevel2Continuation">&lt;object ID&gt;-page&lt;number&gt;-[1|2|3][optional -portrait|-landscape][-&lt;edition&gt;]. v&lt;major&gt;.&lt;minor&gt;</p>
        <p class="exampleLevel1">File name example: 231-page-1-2-landscape-8-v0.4</p>
        <p>The following definitions are used in both conventions:</p>
        <table style="width: 100%;">
            <col style="width: 127px;">
            </col>
            <col>
            </col>
            <tbody>
                <tr>
                    <td>
                        <p><b>Definition</b> <![CDATA[ ]]></p>
                    </td>
                    <td><b>Explanation</b> <![CDATA[ ]]></td>
                </tr>
                <tr>
                    <td>&lt;&gt;</td>
                    <td>Data (filled-in at runtime)</td>
                </tr>
                <tr>
                    <td>[a|b]</td>
                    <td> 	 Choice: a or b</td>
                </tr>
                <tr>
                    <td>[a]</td>
                    <td> 	 Optional: a or nothing</td>
                </tr>
                <tr>
                    <td>portrait	</td>
                    <td>Alternate layout page in portrait orientation</td>
                </tr>
                <tr>
                    <td>landscape	</td>
                    <td>Alternate layout page in landscape orientation</td>
                </tr>
                <tr>
                    <td>major </td>
                    <td>	 Major version number</td>
                </tr>
                <tr>
                    <td>minor </td>
                    <td>	 Minor version number</td>
                </tr>
                <tr>
                    <td>other </td>
                    <td>Literal</td>
                </tr>
            </tbody>
        </table>
        <h2><a name="Version_numbering"></a>Version numbering</h2>
        <p><b>The major version number</b> is increased whenever the object is saved in a status that has the Create Permanent Version option selected (see <a href="../workflows/workflow-status-settings.htm">Workflow Status settings</a>.)</p>
        <p>When the object is saved for statuses that don’t have this option enabled, the <b>minor version number</b> is increased instead.</p>
        <p><b>Permanent versions</b> (such as v1.0, v2.0, v3.0, etc), are never automatically removed. The oldest minor versions are automatically removed when the object outgrows the configured number of versions to track.</p>
        <p>For more information about versioning, see <a href="../configuration/saving-versions.htm">Saving file versions with Enterprise Server</a>. </p>
        <h2><a name="Object_types"></a>Object types</h2>
        <p>For each object type you can have more than one file saved:</p>
        <ul>
            <li class="BulletLevel2SpaceAfter10px" value="1">Layouts, articles, images, and pages have several renditions stored which include the native file, thumbnail files, and preview files.</li>
            <li class="BulletLevel2SpaceAfter10px" value="2">Layouts, articles, and images have versions stored. The number of versions is configurable per object type (see <a href="../configuration/saving-versions.htm">Saving file versions with Enterprise Server</a>).</li>
        </ul>
        <p>For layout objects, all pages reside in the binary .indd file. However, for each of its pages a thumbnail and preview is stored as well. Optionally, an output rendition is stored too, in either PDF or EPS format.</p>
        <table style="width: 100%;mc-table-style: url('../../../resources/tablestyles/example.css');" class="TableStyle-Example" cellspacing="0">
            <col class="Column-Column1">
            </col>
            <tbody>
                <tr class="Body-Body1">
                    <td class="BodyA-Column1-Body1">
                        <p class="tableExample"><b>Example:</b> Assume you have enabled the following features (see <a href="../configuration/page-preview-generation.htm">Page preview generation</a>):</p>
                        <ul>
                            <li class="BulletLevel1Example" value="1">CreatePagePreview</li>
                            <li class="BulletLevel1Example" value="2">CreatePagePDFOnProduce</li>
                        </ul>
                        <p class="tableExample">When saving an InDesign document consisting of 2 pages to a status that has the Output flag enabled, page files are saved in the FileStore as shown below (in the example, the file has been given object id “57”).</p>
                        <table style="width: 100%;">
                            <col>
                            </col>
                            <col>
                            </col>
                            <col>
                            </col>
                            <col>
                            </col>
                            <col>
                            </col>
                            <tbody>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter"><b>File name</b> <![CDATA[ ]]></p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter"><b>Version</b> <![CDATA[ ]]></p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter"><b>File format</b> <![CDATA[ ]]></p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter"><b>Page</b> <![CDATA[ ]]></p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter"><b>Rendition type*</b>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-native.v1.2 </p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Current</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">INDD</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">All</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Native</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-preview.v1.2 </p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Current</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">JPG</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Preview</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-thumb.v1.2 </p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Current</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">JPG</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Thumb</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-native.v1.1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Version 1.1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">INDD</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">All</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Native</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-preview.v1.1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Version 1.1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">JPG</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Preview</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-thumb.v1.1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Version 1.1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">JPG</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Thumb</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-page1-1.v1.2</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Current</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">JPG</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Thumb</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-page1-2.v1.2</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Current</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">JPG</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Preview</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-page1-3.v1.2</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Current</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">PDF</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">1</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Output</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-page2-1.v1.2</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Current</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">JPG</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">2</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Thumb</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-page2-2.v1.2</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Current</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">JPG</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">2</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Preview</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">57-page2-3.v1.2</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Current</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">PDF</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">2</p>
                                    </td>
                                    <td>
                                        <p class="tableExampleNoSpaceBeforeOrAfter">Output</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p class="tableExample">* The Rendition Type is referring to the WDSL definition. Note that both EPS and PDF formats are stored as “output” rendition type. Having both output formats enabled is NOT supported.</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h3><a name="Performance"></a>Performance</h3>
        <p>In order to ensure optimum performance when many files are stored, sub folders are automatically created by the system.</p>
        <p>For instance, if you are about to store more than 100 objects in one folder, the system creates a sub folder named “1”. Files between 100 and 200 are stored inside that folder. The folder creation is done recursively, so if you store the 1000th file in the File Store, a sub folder named “10” is created inside the “10” folder.</p>
        <p>With the exception of the parent “File Store” folder, which you must create manually, Enterprise will create all required sub folders automatically . The newly created sub folders carry the same access attributes as their parent root folder. </p>
        <p>Make sure that the root folder exists and that the access rights are correctly configured. The Web Server user needs the following access rights set: </p>
        <ul>
            <li value="1"><b>Mac OS:</b> “www”</li>
            <li value="2"><b>Windows:</b> “IUSR_&lt;servername&gt;” </li>
            <li value="3"><b>Linux:</b> “nobody”.	 </li>
        </ul>
        <h4>Configuring the number of files stored</h4>
        <p>The following option controls the number of objects stored within one folder  :</p>
        <ul>
            <li value="1"><b>File:</b> configserver.php file </li>
            <li value="2"><b>Name of option:</b> ATTACHMODULO</li>
            <li value="3"><b>Possible values:</b>&#160;any numerical value</li>
            <li value="4"><b>Default setting:</b> 100</li>
            <li value="5"><b>Example:</b> <![CDATA[ ]]></li>
        </ul>
        <p class="exampleLevel1"><code>define ('ATTACHMODULO', 100);</code>
        </p>
        <h2><a name="External_storage"></a>External storage</h2>
        <p>External storage disks can be used for storing files on a different disk than where the Enterprise database is installed. Enterprise allows you to use external drives for the File Store as well as for high-resolution adverts.</p>
        <p>In addition, files can be stored on a remote or integrated third-party content source, such as a wire feed of a news agency.</p>
        <h3><a name="External_File_Store_configuration"></a>External File Store configuration</h3>
        <p>For an example of using a File Store on a system outside the system on which Enterprise Server is installed,see the Knowledge Base article <a href="https://community.woodwing.net/kb/configuring-external-windows-file-store-mac-os-using-apache">Configuring an External Windows File Store for Mac OS Using Apache</a>.</p>
        <p class="note"><b>Note:</b>&#160;Although this is a specific example involving a Mac OS X system with an Apache HTTP Server set up to use a File Store residing on a Windows 2000/XP machine, the same principle can be applied when using other systems.</p>
        <h3><a name="High-resolution_adverts_storage"></a>High-resolution adverts storage</h3>
        <p>When high-resolution adverts are used in a Brand, you are likely to want to keep them in the Enterprise workflow system. Nevertheless, in most cases there is no real need to actually import the (potentially large) files into the File Store. Enterprise Server therefore allows you to configure an external drive. </p>
        <p>This is controlled by the following option:</p>
        <ul>
            <li value="1"><b>File:</b> configserver.php file </li>
            <li value="2"><b>Area:</b> SERVERFEATURES</li>
            <li value="3"><b>Name of option:</b> HighResStoreMac or HighResStoreWin</li>
            <li value="4"><b>Possible values:</b>&#160;the path to your external drive</li>
            <li value="5"><b>Example:</b> <![CDATA[ ]]></li>
        </ul>
        <p class="exampleLevel1"><code>new Feature( 'HighResStoreMac', 'Volumes/HighResFileStore/' ),</code>
        </p>
        <p class="note"><b>Note:</b> For more detailed information about these settings, see the article “How to Configure External Storage for Highres Adverts” on the WoodWing Knowledge Base: https://community.woodwing.net/kb/file-storage-configuring-storage-highres-adverts.</p>
        <h3><a name="3rd-Party_content_sources"></a>3rd-Party content sources</h3>
        <p>Enterprise can be integrated with 3rd-party content sources such as Wire feeds of news agencies. The connection is made through a Content Source plug-in which is managed through the Server Plug-Ins Maintenance page. See <a href="../server-plugins/about-enterprise-server-plugins.htm">Enterprise Server Plug-ins</a>.</p>
        <h2><a name="Storing_layouts_and_articles_locally"></a>Storing layouts and articles locally or always on the Server</h2>
        <p>The default behavior of the Smart Connection plug-ins for InDesign and InCopy is to always store native files (InDesign and InCopy documents) to the local hard disk without sending them to the server. This is done to reduce network traffic and speed up intermediate save operations.</p>
        <p>Enterprise Server can be configured in such a way that the file is always saved directly to the database instead of locally. For more information, see <a href="../../smart-connection/configuration/always-save-layout-or-article-to-database.htm" class="MCXref xref">Always save a layout or article to Enterprise using Smart Connection</a>.</p>
        <div class="MCRelationshipsProxy_0">
            <p class="conceptHeading_concept_0">Related Information</p>
            <p class="conceptItem_concept_0"><a href="number-of-stored-files-in-file-store.htm">Controlling the number of files stored in the File Store of  Enterprise Server 9</a>
            </p>
            <p class="conceptItem_concept_0"><a href="configuring-storage-for-high-res-adverts.htm">Configuring storage for high-resolution adverts in Enterprise Server 9</a>
            </p>
            <p class="conceptItem_concept_0"><a href="cleaning-up-temp-files.htm">Cleaning up temp files in Enterprise</a>
            </p>
            <p class="conceptItem_concept_0"><a href="configuring-an-external-windows-file-store-for-mac.htm">Configuring an external Windows file store for Mac OS using Apache </a>
            </p>
        </div>
    </body>
</html>